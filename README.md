###WASHING MACHINE######

The task is to design and implement Washing Machine Simulation using REST API

Bitbucket url-https://bitbucket.org/kunalkeshri/washingmachine/src/master/

###MAVEN INSTALLATION AND RUN#############

$ mvn spring-boot:run


###RESFUL API DISCRIPTION##########

http://localhost:6060/swagger-ui.html#/

GET  /api/washingmachines - Get Washing Machine Resources

GET /api/washingmachines/{id} - Get Washing Machine Resource by Id

POST /api/washingmachines - Create Washing Machine Resource

DELETE /api/washingmachines/{id} - Delete Washing Machine Resource

PUT /api/washingmachines/{id} - Update Washing Machine Resource by Id

GET /api/washingstate - Get Current State of Washing Machine

PATCH /api/washingmachines/{id} -Partial Update Washing Machine Resource by Id

###IN MEMORY DATABASE########

login url-http://localhost:6060/console/
jdbc url -jdbc:h2:mem:testdb  
username=sa
password=sa

###TABLE DISCRIPTION########

TBL_WASHINING_MACHINE-contains washing machine data
TBL_WM_OPERATION -conatins operation data
TBL_WM_PROGRAM ---contains washing machine program data

#### SAMPLE REST API DATA###############
###CREATE RESOURCE###########

 {
  
        "modelNumber": "LG3245",
        "serialNumber": "SN-765432156",
        "programs": [
            {
                "dry": {
                    
                    "timer": 10000,
                    "rpm": 40,
                    "temperature": 10
                },
                "wash": {
                  
                    "timer": 20000,
                    "rpm": 40,
                    "temperature": 30
                },
                "drain": {
                   
                    "timer": 5000,
                    "rpm": 40,
                    "temperature": 10
                }
            }
            
        ]
    }
    
###UPDATE RESOURCE###########
    {
  
        "modelNumber": "SAM3246",
        "serialNumber": "SN-12345",
        "programs": [
            {
                "dry": {
                    
                    "timer": 70000,
                    "rpm": 90,
                    "temperature": 60
                },
                "wash": {
                  
                    "timer": 26000,
                    "rpm": 140,
                    "temperature": 60
                },
                "drain": {
                   
                    "timer": 4000,
                    "rpm": 30,
                    "temperature": 70
                }
            }
            
        ]
    }


##############################################################







