package com.example.washingmachine.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.washingmachine.domain.WashingMachine;
import com.example.washingmachine.service.WashingMachineService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
@Api(value = "Washing Machine Controller")
public class WashingMachineController {

	@Autowired
	private WashingMachineService washingMachineService;

	@ApiOperation(value = "Get Washing Machine Resources", response = List.class)
	@GetMapping("/washingmachines")
	public @ResponseBody ResponseEntity<List<WashingMachine>> washingmachines() {
		return new ResponseEntity<>(washingMachineService.getWashingMachines(), HttpStatus.OK);
	}

	@ApiOperation(value = "Get Washing Machine Resource by Id", response = WashingMachine.class)
	@GetMapping("/washingmachines/{id}")
	public @ResponseBody ResponseEntity<WashingMachine> getWashingMachineById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(washingMachineService.getWashingMachineById(id), HttpStatus.OK);
	}

	@ApiOperation(value = "Create Washing Machine Resource", response = WashingMachine.class)
	@PostMapping("/washingmachines")
	public @ResponseBody ResponseEntity<WashingMachine> addWashingMachine(@RequestBody WashingMachine washingMachine) {
		return new ResponseEntity<>(washingMachineService.save(washingMachine), HttpStatus.CREATED);
	}

	@ApiOperation(value = "Delete Washing Machine Resource", response = Void.class)
	@DeleteMapping("/washingmachines/{id}")
	public @ResponseBody ResponseEntity<?> deleteWashingMachineById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(washingMachineService.deleteWashingMachineById(id), HttpStatus.ACCEPTED);
		
	}
	
	@ApiOperation(value = "Update Washing Machine Resource by Id", response = WashingMachine.class)
	@PutMapping("/washingmachines/{id}")
	public @ResponseBody ResponseEntity<WashingMachine> updateWashingMachineById(@PathVariable("id") Long id,
			@RequestBody WashingMachine washingMachine) {
		return new ResponseEntity<>(washingMachineService.updateWashingMachineById(id, washingMachine), HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Partial Update Washing Machine Resource by Id", response = WashingMachine.class)
	@PatchMapping("/washingmachines/{id}")
	public @ResponseBody ResponseEntity<?> partialUpdateWashingMachineById(@PathVariable("id") Long id,
			@RequestBody WashingMachine washingMachine) {
		return washingMachineService.partialUpdateWashingMachineById(id, washingMachine);
	}
	
	@ApiOperation(value = "Get Current State of Washing Machine", response = String.class)
	@GetMapping("/washingstate")
	public @ResponseBody String currentState() {
		return washingMachineService.getWashingStatus();
	}
	
	

}
