package com.example.washingmachine.domain;

import javax.persistence.Entity;

@Entity
public class Drain extends WMOperation {

	public Drain() {
	}

	public Drain(Long timer, Integer rpm, Integer temperature) {
		super(timer, rpm, temperature);
	}

	@Override
	public String toString() {
		return "Drain :" + super.toString();
	}

}