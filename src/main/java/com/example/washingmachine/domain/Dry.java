package com.example.washingmachine.domain;

import javax.persistence.Entity;

@Entity
public class Dry extends WMOperation {

	public Dry() {
	}

	public Dry(Long timer, Integer rpm, Integer temperature) {
		super(timer, rpm, temperature);
	}

	@Override
	public String toString() {
		return "Dry :" + super.toString();
	}

}
