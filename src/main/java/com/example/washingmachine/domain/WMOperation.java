package com.example.washingmachine.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_WM_OPERATION")
public class WMOperation {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "timer")
	private Long timer;

	@Column(name = "rpm")
	private Integer rpm;

	@Column(name = "temp")
	private Integer temperature;

	public WMOperation() {
		super();
	}

	public WMOperation(Long timer, Integer rpm, Integer temperature) {
		super();
		this.timer = timer;
		this.rpm = rpm;
		this.temperature = temperature;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTimer() {
		return timer;
	}

	public void setTimer(Long timer) {
		this.timer = timer;
	}

	public Integer getRpm() {
		return rpm;
	}

	public void setRpm(Integer rpm) {
		this.rpm = rpm;
	}

	public Integer getTemperature() {
		return temperature;
	}

	public void setTemperature(Integer temperature) {
		this.temperature = temperature;
	}

	@Override
	public String toString() {
		return "WMOperation [ timer=" + timer + ", rpm=" + rpm + ", temperature=" + temperature + "]";
	}
	
	

}
