package com.example.washingmachine.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_WM_PROGRAM")
public class WMProgram {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "dry_program", referencedColumnName = "id")
	private Dry dry;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "wash_program", referencedColumnName = "id")
	private Wash wash;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "drain_program", referencedColumnName = "id")
	private Drain drain;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Dry getDry() {
		return dry;
	}

	public void setDry(Dry dry) {
		this.dry = dry;
	}

	public Wash getWash() {
		return wash;
	}

	public void setWash(Wash wash) {
		this.wash = wash;
	}

	public Drain getDrain() {
		return drain;
	}

	public void setDrain(Drain drain) {
		this.drain = drain;
	}

	@Override
	public String toString() {
		return "WMProgram [dry=" + dry + ", wash=" + wash + ", drain=" + drain + "]";
	}

}
