package com.example.washingmachine.domain;

import javax.persistence.Entity;

@Entity
public class Wash extends WMOperation {

	public Wash() {
	}

	public Wash(Long timer, Integer rpm, Integer temperature) {
		super(timer, rpm, temperature);
	}

}
