package com.example.washingmachine.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_WASHINING_MACHINE")
public class WashingMachine {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "modelNumber")
	private String modelNumber;

	@Column(name = "serialNumber")
	private String serialNumber;

	@Column(name = "programs")
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "washing_machine_id", referencedColumnName = "id")
	private List<WMProgram> programs;

	public WashingMachine() {

	}

	public WashingMachine(String modelNumber, String serialNumber, List<WMProgram> programs) {
		this.modelNumber = modelNumber;
		this.serialNumber = serialNumber;
		this.programs = programs;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public List<WMProgram> getPrograms() {
		return programs;
	}

	public void setPrograms(List<WMProgram> programs) {
		this.programs = programs;
	}

	@Override
	public String toString() {
		return "WashingMachine [ modelNumber=" + modelNumber + ", serialNumber=" + serialNumber + ", programs="
				+ programs + "]";
	}

}
