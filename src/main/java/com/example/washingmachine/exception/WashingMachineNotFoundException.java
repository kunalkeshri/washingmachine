package com.example.washingmachine.exception;

public class WashingMachineNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public WashingMachineNotFoundException(String message) {
		super(message);

	}

}
