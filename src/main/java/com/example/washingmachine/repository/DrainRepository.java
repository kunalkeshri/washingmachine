package com.example.washingmachine.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.washingmachine.domain.Drain;

@Repository
public interface DrainRepository extends JpaRepository<Drain, Long> {
	
}