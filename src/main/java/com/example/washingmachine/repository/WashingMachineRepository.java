package com.example.washingmachine.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.washingmachine.domain.WashingMachine;

@Repository
@SuppressWarnings("unchecked")
public interface WashingMachineRepository extends JpaRepository<WashingMachine, Long> {
	
	List<WashingMachine> findAll();

	WashingMachine save(WashingMachine appliance);
}