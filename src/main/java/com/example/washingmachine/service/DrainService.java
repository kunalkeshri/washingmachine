package com.example.washingmachine.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.washingmachine.domain.Drain;
import com.example.washingmachine.repository.DrainRepository;

@Service
public class DrainService {

	@Autowired
	private DrainRepository drainRepository;

	public Drain save(Drain drain) {
		return drainRepository.save(drain);
	}
}
