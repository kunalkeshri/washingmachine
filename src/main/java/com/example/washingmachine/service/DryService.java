package com.example.washingmachine.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.washingmachine.domain.Dry;
import com.example.washingmachine.repository.DryRepository;

@Service
public class DryService {

	@Autowired
	private DryRepository dryRepository;

	public Dry save(Dry dry) {
		return dryRepository.save(dry);
	}
}
