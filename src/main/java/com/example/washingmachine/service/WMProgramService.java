package com.example.washingmachine.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.washingmachine.domain.WMProgram;
import com.example.washingmachine.repository.WMProgramRepository;

@Service
public class WMProgramService {

	@Autowired
	private WMProgramRepository wmProgramRepository;

	public WMProgram save(WMProgram program) {
		return wmProgramRepository.save(program);
	}
	
	public List<WMProgram> saveAll(List<WMProgram> programs) {
		return wmProgramRepository.saveAll(programs);
	}
}
