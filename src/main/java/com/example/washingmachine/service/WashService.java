package com.example.washingmachine.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.washingmachine.domain.Wash;
import com.example.washingmachine.repository.WashRepository;

@Service
public class WashService {

	@Autowired
	private WashRepository washRepository;

	public Wash save(Wash wash) {
		return washRepository.save(wash);
	}
}
