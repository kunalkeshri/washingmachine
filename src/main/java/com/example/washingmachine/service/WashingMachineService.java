package com.example.washingmachine.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.washingmachine.domain.WashingMachine;
import com.example.washingmachine.exception.DBException;
import com.example.washingmachine.exception.WashingMachineNotFoundException;
import com.example.washingmachine.repository.WashingMachineRepository;
import com.example.washingmachine.utils.WashingStates;

@Service
public class WashingMachineService {

	@Autowired
	private WashingMachineRepository washingMachineRepository;

	private StringBuffer washingStatus = new StringBuffer(WashingStates.WAITING.toString());
	private static boolean isRunning=false;;

	public List<WashingMachine> getWashingMachines() {
		return Optional.ofNullable(washingMachineRepository.findAll()).filter(a -> !a.isEmpty())
				.orElseThrow(() -> new WashingMachineNotFoundException("Washing Machine Not Found"));

	}

	public WashingMachine getWashingMachineById(long id) {
		return washingMachineRepository.findById(id)
				.orElseThrow(() -> new WashingMachineNotFoundException("Washing Machine Not Found"));

	}

	public WashingMachine save(WashingMachine washingMachine) {
		return Optional.ofNullable(washingMachineRepository.save(washingMachine))
				.orElseThrow(() -> new DBException("DB Exception"));

	}

	public String deleteWashingMachineById(Long id) {
		return washingMachineRepository.findById(id).map(x -> {
			washingMachineRepository.deleteById(id);
			return "Resource Deleted Succesfully";
		}).orElseThrow(() -> new WashingMachineNotFoundException("Washing Machine Not Found"));
	}

	public WashingMachine updateWashingMachineById(Long id, WashingMachine washingMachine) {

		return washingMachineRepository.findById(id).map(wm -> {
			wm.setModelNumber(washingMachine.getModelNumber());
			wm.setSerialNumber(washingMachine.getSerialNumber());
			wm.setPrograms(washingMachine.getPrograms());
			return wm;
		}).map(washingMachineRepository::save)
				//.map(wm -> ResponseEntity.ok().body(wm))
				.orElseThrow(() -> new WashingMachineNotFoundException("Washing Machine Not Found"));
	}

	public ResponseEntity<?> partialUpdateWashingMachineById(Long id, WashingMachine washingMachine) {

		return washingMachineRepository.findById(id).map(wm -> {
			wm.setModelNumber(washingMachine.getModelNumber());
			wm.setSerialNumber(washingMachine.getSerialNumber());
			wm.setPrograms(washingMachine.getPrograms());
			return wm;
		}).map(washingMachineRepository::save).map(wm -> ResponseEntity.ok().body(wm))
				.orElseThrow(() -> new WashingMachineNotFoundException("Washing Machine Not Found"));
	}

	public String getWashingStatus() {
		return washingStatus.toString();
	}

	public void startMachine() {
		if (!isRunning) {
			washingStatus = new StringBuffer(WashingStates.RUNNING.toString());
		}
	}

	public void stopMachine() {
		if (isRunning) {
			washingStatus = new StringBuffer(WashingStates.STOPPED.toString());
		}
	}

}
