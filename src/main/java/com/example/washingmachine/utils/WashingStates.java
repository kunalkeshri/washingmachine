package com.example.washingmachine.utils;

public enum WashingStates {
	RUNNING, FINISHED, STOPPED, WAITING;
}