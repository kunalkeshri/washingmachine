package com.example.washingmachine.test.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.example.washingmachine.controller.WashingMachineController;
import com.example.washingmachine.domain.WashingMachine;
import com.example.washingmachine.service.WashingMachineService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
public class WashingMachineControllerTest {

	private MockMvc mockMvc;

	@InjectMocks
	WashingMachineController washingMachineController;

	@Mock
	private WashingMachineService washingMachineService;

	ObjectMapper mapper = new ObjectMapper();

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(washingMachineController).build();
	}

	@Test
	public void getWashingMachines() throws Exception {

		String jsonStringArray = "[{\"id\":1,\"modelNumber\":\"LG3245\",\"serialNumber\":\"SN-765432156\",\"programs\":[{\"id\":2,\"dry\":{\"id\":4,\"timer\":10000,\"rpm\":40,\"temperature\":10},\"wash\":{\"id\":5,\"timer\":20000,\"rpm\":40,\"temperature\":30},\"drain\":{\"id\":3,\"timer\":5000,\"rpm\":40,\"temperature\":10}}]}]";

		List<WashingMachine> wmList = Arrays.asList(mapper.readValue(jsonStringArray, WashingMachine[].class));

		Mockito.when(washingMachineService.getWashingMachines()).thenReturn(wmList);
		mockMvc.perform(MockMvcRequestBuilders.get("/api/washingmachines"))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void getWashingMachineById() throws Exception {

		String jsonStringObj = "{\"id\":1,\"modelNumber\":\"LG3245\",\"serialNumber\":\"SN-765432156\",\"programs\":[{\"id\":2,\"dry\":{\"id\":4,\"timer\":10000,\"rpm\":40,\"temperature\":10},\"wash\":{\"id\":5,\"timer\":20000,\"rpm\":40,\"temperature\":30},\"drain\":{\"id\":3,\"timer\":5000,\"rpm\":40,\"temperature\":10}}]}";

		WashingMachine wmObj = mapper.readValue(jsonStringObj, WashingMachine.class);

		Mockito.when(washingMachineService.getWashingMachineById(1)).thenReturn(wmObj);

		mockMvc.perform(MockMvcRequestBuilders.get("/api/washingmachines/1").accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.modelNumber", Matchers.is("LG3245")))
				.andExpect(MockMvcResultMatchers.jsonPath("$.serialNumber", Matchers.is("SN-765432156")));

		Mockito.verify(washingMachineService).getWashingMachineById(1l);
	}

	@Test
	public void addWashingMachine() throws Exception {

		String jsonStringObj = "{\"modelNumber\":\"LG3245\",\"serialNumber\":\"SN-765432156\",\"programs\":[{\"dry\":{\"timer\":10000,\"rpm\":40,\"temperature\":10},\"wash\":{\"timer\":20000,\"rpm\":40,\"temperature\":30},\"drain\":{\"timer\":5000,\"rpm\":40,\"temperature\":10}}]}";
		WashingMachine wmObj = mapper.readValue(jsonStringObj, WashingMachine.class);
		Mockito.when(washingMachineService.updateWashingMachineById(1l, wmObj)).thenReturn(Mockito.any());

		mockMvc.perform(MockMvcRequestBuilders.post("/api/washingmachines").contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).content(jsonStringObj))
				.andExpect(MockMvcResultMatchers.status().isCreated());

	}

	@Test
	public void updateWashingMachine() throws Exception {

		String jsonStringObj = "{\"modelNumber\":\"LG3245\",\"serialNumber\":\"SN-765432156\",\"programs\":[{\"dry\":{\"timer\":10000,\"rpm\":40,\"temperature\":10},\"wash\":{\"timer\":20000,\"rpm\":40,\"temperature\":30},\"drain\":{\"timer\":5000,\"rpm\":40,\"temperature\":10}}]}";

		WashingMachine wmObj = mapper.readValue(jsonStringObj, WashingMachine.class);
		Mockito.when(washingMachineService.updateWashingMachineById(1l, wmObj)).thenReturn(getWashingMachineStubData());

		mockMvc.perform(MockMvcRequestBuilders.put("/api/washingmachines/1").contentType(MediaType.APPLICATION_JSON)
				.content(jsonStringObj)).andExpect(MockMvcResultMatchers.status().isOk());

	}

	@Test
	public void deleteWashingMachineById() throws Exception {
		Mockito.when(washingMachineService.deleteWashingMachineById(1l)).thenReturn(Mockito.anyString());
		mockMvc.perform(MockMvcRequestBuilders.delete("/api/washingmachines/1").accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isAccepted());
	}

	private WashingMachine getWashingMachineStubData() throws JsonParseException, JsonMappingException, IOException {
		String jsonStringObj = "{\"modelNumber\":\"LG3245\",\"serialNumber\":\"SN-765432156\",\"programs\":[{\"dry\":{\"timer\":10000,\"rpm\":40,\"temperature\":10},\"wash\":{\"timer\":20000,\"rpm\":40,\"temperature\":30},\"drain\":{\"timer\":5000,\"rpm\":40,\"temperature\":10}}]}";

		return mapper.readValue(jsonStringObj, WashingMachine.class);
	}
}
